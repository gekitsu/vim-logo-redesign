# Custom Vim Logo

Nothing much to say, really. Just an attempt at redesigning the Vim logo because it looks a bit stale and I had to procrastinate.

![comparison: 60 px original logo, vectorisation from wikipedia, my redesign](https://github.com/gekitsu/vim-logo-redesign/blob/master/pics/comparison_60px_svg-wiki_nvl-2tone.png?raw=true)

There are a few variations on the logo: `_1tone` variants are reduced to just one flat tone, `_2tone` mirrors the original logo’s green and grey elements, and `_allthetones` is a quick attempt at reproducing the original pixel art’s gaudy bevel edges.

In the `pics` directory, I put the comparison picture above, with the original pixeled 60 px version, the rather shoddily vectorised version from [Wikipedia](https://en.wikipedia.org/wiki/File:Vimlogo.svg), and the two-tone version of my redesign. I also exported it at different sized to illustrate where it begins to fall apart unless tweaked by hand. I’d say it just so works at 64 px, requires touch-ups at 32 px and pretty much falls apart at 16 px.

My original working files are in the `source` directory.
